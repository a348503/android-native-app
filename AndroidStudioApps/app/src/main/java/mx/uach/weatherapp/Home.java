package mx.uach.weatherapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.logging.Logger;

public class Home extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        final Button btnCUU = findViewById(R.id.cuu);
        btnCUU.setOnClickListener(new View.OnClickListener() { //esto es una sobrecarga desde la instancia y no desde la clase
            @Override
            public void onClick(View v) {
                Logger.getLogger("app ->").info("presioné un botón");
                //INTEND ES UNA ACTIVIDAD VIVA PARA AL MOMENTO DE CAMBIAR HAYA UNA VIVA Y LA APP NO MUERA
                //ES COMO EL PUENTE ENTRE AMBAS ACTIVIDADES
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtra("CITY","Shiwawa");
                startActivity(intent);
            }
        });
    }

    public void goToPrr(View v){
        Logger.getLogger("app -> ").warning("presione Parral");
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.putExtra("CITY","Parral la capital de mundo");
        startActivity(intent);
    }
}