# Weather Appp

This litle project is a native Android app wich simulates a weather app.

## Getting Started

Just pull the repo and open the folder 'app' in android studio.

### Prerequisites

-Android Studio
-Java JDK
-AVD


### And coding style tests

-Cammel Case
-Upper Camel Case


## Built With

* Android Studio
* Java

## Contributing

No contributions allowed.

## Versioning

v1.0.0 

## Authors

* **Gilberto Contreras Conn** - 348503


## License

This is a school project so there is no license.

## Acknowledgments

* Teacher: I. S. Luis Antonio Ramírez Martínez.
